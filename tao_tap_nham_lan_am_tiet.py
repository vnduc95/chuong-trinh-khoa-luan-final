# -*- coding: utf-8 -*-

from __future__ import print_function
import re
import sys


# =============================== xoa dau ========================================
patterns = {
    'á': 'a',
    'à': 'a',
    'ả': 'a',
    'ạ': 'a',
    'ã': 'a',
    'ắ': 'ă',
    'ằ': 'ă',
    'ẳ': 'ă',
    'ẵ': 'ă',
    'ặ': 'ă',
    'ấ': 'â',
    'ầ': 'â',
    'ẩ': 'â',
    'ẫ': 'â',
    'ậ': 'â',
    'é': 'e',
    'è': 'e',
    'ẻ': 'e',
    'ẽ': 'e',
    'ẹ': 'e',
    'ế': 'ê',
    'ề': 'ê',
    'ể': 'ê',
    'ễ': 'ê',
    'ệ': 'ê',
    'í': 'i',
    'ì': 'i',
    'ỉ': 'i',
    'ĩ': 'i',
    'ị': 'i',
    'ó': 'o',
    'ò': 'o',
    'ỏ': 'o',
    'õ': 'o',
    'ọ': 'o',
    'ố': 'ô',
    'ồ': 'ô',
    'ổ': 'ô',
    'ỗ': 'ô',
    'ộ': 'ô',
    'ớ': 'ơ',
    'ờ': 'ơ',
    'ở': 'ơ',
    'ỡ': 'ơ',
    'ợ': 'ơ',
    'ú': 'u',
    'ù': 'u',
    'ủ': 'u',
    'ũ': 'u',
    'ụ': 'u',
    'ứ': 'ư',
    'ừ': 'ư',
    'ử': 'ư',
    'ữ': 'ư',
    'ự': 'ư',
    'ý': 'y',
    'ỳ': 'y',
    'ỷ': 'y',
    'ỹ': 'y',
    'ỵ': 'y'
}
def convert(text):
    output = text
    for regex, replace in patterns.items():
        output = re.sub(regex, replace, output)
        # deal with upper case
        # output = re.sub(regex.upper(), replace.upper(), output)
    return output
# =============================== ket thuc xoa dau ==================================

# ================================== them dau =======================================
patterns_them_dau = [
	['oai', 'oái', 'oài', 'oải', 'oãi', 'oại'],
	['oay', 'oáy', 'oày', 'oảy', 'oãy', 'oạy'],
	['uây', 'uấy', 'uầy', 'uẩy', 'uẫy', 'uậy'],
	['uôi', 'uối', 'uồi', 'uổi', 'uỗi', 'uội'],
	['iêu', 'iếu', 'iều', 'iểu', 'iễu', 'iệu'],
	['uyê', 'uyế', 'uyề', 'uyể', 'uyễ', 'uyệ'],
	['ươu', 'ướu', 'ườu', 'ưởu', 'ưỡu', 'ượu'],
	['ươi', 'ưới', 'ười', 'ưởi', 'ưỡi', 'ượi'],
	['uya', 'uýa', 'uỳa', 'uỷa', 'uỹa', 'uỵa'],
	['yêu', 'yếu', 'yều', 'yểu', 'yễu', 'yệu'],
	['uyu', 'uýa', 'uỳa', 'uỷa', 'uỹa', 'uỵa'],
	['ai', 'ái', 'ài', 'ải', 'ãi', 'ại'],
	['ao', 'áo', 'ào', 'ảo', 'ão', 'ạo'],
	['au', 'áu', 'àu', 'ảu', 'ãu', 'ạu'],
	['ay', 'áy', 'ày', 'ảy', 'ãy', 'ạy'],
	['âu', 'ấu', 'ầu', 'ẩu', 'ẫu', 'ậu'],
	['ây', 'ấy', 'ầy', 'ẩy', 'ẫy', 'ậy'],
	['eo', 'éo', 'èo', 'ẻo', 'ẽo', 'ẹo'],
	['êu', 'ếu', 'ều', 'ểu', 'ễu', 'ệu'],
	['ia', 'ía', 'ìa', 'ỉa', 'ĩa', 'ịa'],
	['iu', 'íu', 'ìu', 'ỉu', 'ĩu', 'ịu'],
	['iê', 'iế', 'iề', 'iể', 'iễ', 'iệ'],
	['oa', 'óa', 'òa', 'ỏa', 'õa', 'ọa'],
	['oă', 'oắ', 'oằ', 'oẳ', 'oẵ', 'oặ'],
	['oe', 'óe', 'òe', 'ỏe', 'õe', 'ọe'],
	['oi', 'ói', 'òi', 'ỏi', 'õi', 'ọi'],
	['oo'],
	['ôi', 'ối', 'ồi', 'ổi', 'ỗi', 'ội'],
	['ơi', 'ới', 'ời', 'ởi', 'ỡi', 'ợi'],
	['ua', 'úa', 'ùa', 'ủa', 'ũa', 'ụa'],
	['uâ', 'uấ', 'uầ', 'uẩ', 'uẫ', 'uậ'],
	['ui', 'úi', 'ùi', 'ủi', 'ũi', 'ụi'],
	['uê', 'uế', 'uề', 'uể', 'uễ', 'uệ'],
	['uô', 'uố', 'uồ', 'uổ', 'uỗ', 'uộ'],
	['uơ', 'uớ', 'uờ', 'uở', 'uỡ', 'uợ'],
	['uy', 'úy', 'ùy', 'ủy', 'ũy', 'ụy'],
	['ưa', 'ứa', 'ừa', 'ửa', 'ữa', 'ựa'],
	['ưi', 'ứi', 'ừi', 'ửi', 'ữi', 'ựi'],
	['ươ', 'ướ', 'ườ', 'ưở', 'ưỡ', 'ượ'],
	['ưu', 'ứu', 'ừu', 'ửu', 'ữu', 'ựu'],
	['yê', 'yế', 'yề', 'yể', 'yễ', 'yệ'],
    ['a', 'á', 'à', 'ả', 'ã', 'ạ'],
	['ă', 'ắ', 'ằ', 'ẳ', 'ẵ', 'ặ'],
	['â', 'ấ', 'ầ', 'ẩ', 'ẫ', 'ậ'],
	['e', 'é', 'è', 'ẻ', 'ẽ', 'ẹ'],
	['ê', 'ế', 'ề', 'ể', 'ễ', 'ệ'],
	['i', 'í', 'ì', 'ỉ', 'ĩ', 'ị'],
	['o', 'ó', 'ò', 'ỏ', 'õ', 'ọ'],
	['ô', 'ố', 'ồ', 'ổ', 'ỗ', 'ộ'],
	['ơ', 'ớ', 'ờ', 'ở', 'ỡ', 'ợ'],
	['u', 'ú', 'ù', 'ủ', 'ũ', 'ụ'],
	['ư', 'ứ', 'ừ', 'ử', 'ữ', 'ự'],
	['y', 'ý', 'ỳ', 'ỷ', 'ỹ', 'ỵ']
]

def them_dau(text):
	rs = []
	for i in xrange(len(patterns_them_dau)):
		if patterns_them_dau[i][0] in text:
			for j in xrange(len(patterns_them_dau[i])):
				new_word = text.replace(patterns_them_dau[i][0], patterns_them_dau[i][j])
				rs.append(new_word)
			return rs

# ================================== ket thuc them dau =====================================

phu_am_dau = [
				['ngh', 'ng', 'nh', 'gh'],
				['tr', 't', 'r', 'th', 'ch'],
				['th', 't', 'h', 'ch', 'nh', 'gh', 'kh', 'ph', 'tr'],
				['qu', 'q', 'c'],
				['ph', 'ch', 'gh', 'p', 'h', 'nh', 'kh', 'th'],
				['nh', 'ng', 'n', 'h', 'ngh'],
				['ng', 'ngh', 'n', 'g', 'nh'],
				['kh', 'k', 'h', 'ch', 'gh', 'th', 'ph'],
				['gi', 'gh', 'g', 'r', 'd', 'gh'],
				['gh', 'g', 'h', 'ch', 'ngh', 'th', 'nh', 'kh', 'ph', 'gi'],
				['ch', 'c', 'h', 'tr', 'th', 'nh', 'gh', 'kh', 'ph'],
				['b', 'p', 'v', 'n'],
				['c', 'ch', 'x', 'v', 'qu'],
				['d', 'đ', 'r', 'gi'],
				['đ', 'd', 'r', 'gi'],
				['dd', 'd', 'r', 'gi'],
				['g', 'gi', 'gh', 'ng', 'r', 'd', 'h'],
				['h', 'g', 'nh', 'ch', 'gh', 'ph', 'kh'],
				['k', 'l', 'kh', 'c'],
				['l', 'k', 'n'],
				['m', 'n'],
				['n', 'l', 'nh', 'ng'],
				['p', 'ph', 'b', 'q'],
				['q', 'qu', 'p'],
				['r', 'd', 'gi', 't', 'tr'],
				['s', 'x', 'd'],
				['t', 'r', 'tr', 'th'],
				['v', 'c', 'b'],
				['x', 's', 'c'],
				['f', 'ph'],
				['j', 'gi']
			]
nguyen_am = [
				['a', 'ă', 'â', 'ai', 'ao', 'au', 'ay', 'ia', 'oa', 'ua', 'ưa'],
				['ă', 'a', 'â', 'oă'],
				['aw', 'ă', 'a', 'â', 'oă'],
				['â', 'a', 'ă', 'âu', 'ây', 'uâ'],
				['aa', 'â', 'a', 'ă', 'âu', 'ây', 'uâ'],
				['e', 'ê', 'eo', 'oe'],
				['ê', 'e', 'iê', 'yê', 'uê', 'êu'],
				['ee', 'ê', 'e', 'iê', 'yê', 'uê', 'êu'],
				['i', 'u', 'o', 'y', 'ai', 'ia', 'iê', 'oi', 'ôi', 'ơi', 'ui', 'ưi'],
				['o', 'u', 'i', 'ô', 'ơ', 'ao', 'eo', 'oa', 'oă','oe', 'oi', 'oo'],
				['ô', 'o', 'ơ', 'i', 'u', 'ôi', 'uô'],
				['oo', 'ô', 'o', 'ơ', 'i', 'u', 'ôi', 'uô'],
				['ơ', 'o', 'ô', 'i', 'ư', 'ơi', 'uơ', 'ươ'],
				['ow', 'ơ', 'o', 'ô', 'i', 'ư', 'ơi', 'uơ', 'ươ'],
				['u', 'y', 'ư', 'i', 'o', 'au', 'êu', 'iu', 'ua', 'uâ', 'ui', 'uê', 'uô', 'uơ', 'uy'],
				['ư', 'u', 'i', 'ơ', 'ưa', 'ưi', 'ươ', 'ưu'],
				['uw', 'ư', 'u', 'i', 'ơ', 'ưa', 'ưi', 'ươ', 'ưu'],
				['y', 'u', 'i', 'ay', 'ây', 'uy', 'yê'],
				['ai', 'a', 'i', 'au', 'ay', 'ao', 'oai', 'oay', 'ia', 'oa', 'ua'],
				['ao', 'a', 'o', 'ai', 'au', 'ay', 'oă', 'oa', 'ia', 'oai', 'oay'],
				['au', 'a', 'u', 'ai', 'ay', 'ao', 'ia', 'ua', 'oa', 'âu', 'uâ'],
				['ay', 'a', 'y', 'au', 'ai', 'ây', 'oay', 'oai', 'uya'],
				['âu', 'â', 'u', 'au', 'ây', 'uâ', 'uây'],
				['aau', 'âu', 'â', 'u', 'au', 'ây', 'uâ', 'uây'],
				['ây', 'â', 'y', 'ay', 'âu', 'uây'],
				['aay', 'ây', 'â', 'y', 'ay', 'âu', 'uây'],
				['eo', 'e', 'o', 'êu', 'oe'],
				['êu', 'ê', 'u', 'eo', 'uê', 'iêu'],
				['eeu', 'êu', 'ê', 'u', 'eo', 'uê', 'iêu'],
				['ia', 'i', 'a', 'ua', 'oa', 'ai'],
				['iu', 'i', 'u', 'ui'],
				['iê', 'i', 'ê', 'uê', 'iêu', 'yê'],
				['iee', 'iê', 'i', 'ê', 'uê', 'iêu', 'yê'],
				['oa', 'o', 'a', 'ia', 'ao', 'oai', 'oay'],
				['oă', 'o', 'ă', 'oa', 'oai', 'oay'],
				['oaw', 'oă', 'o', 'ă', 'oa', 'oai', 'oay'],
				['oe', 'o', 'e', 'eo'],
				['oi', 'i', 'o', 'oai', 'oo', 'ôi', 'ơi'],
				['oo', 'o', 'ô', 'ơ'],
				['ôi', 'ô', 'i', 'ơi', 'oi', 'uôi'],
				['ooi', 'ôi', 'ô', 'i', 'ơi', 'oi', 'uôi'],
				['ơi', 'ơ', 'i', 'oi', 'ôi'],
				['owi', 'ơi', 'ơ', 'i', 'oi', 'ôi'],
				['ua', 'u', 'a', 'ia', 'au', 'ưa', 'uya', 'uâ', 'uây', 'ia'],
				['uâ', 'u', 'â', 'ua', 'âu', 'uây', 'ây'],
				['uaa', 'uâ', 'u', 'â', 'ua', 'âu', 'uây', 'ây'],
				['ui', 'u', 'i', 'iu', 'uy', 'uôi', 'ưi'],
				['uê', 'u', 'ê', 'iê', 'êu', 'uyê', 'yê'],
				['uee', 'uê', 'u', 'ê', 'iê', 'êu', 'uyê', 'yê'],
				['uô', 'u', 'ô', 'uơ', 'ươ', 'ươu', 'ươi'],
				['uoo', 'uô', 'u', 'ô', 'uơ', 'ươ', 'ươu', 'ươi'],
				['uơ', 'u', 'ơ', 'uô', 'ươ', 'ươi', 'ươu'],
				['uow', 'uơ', 'u', 'ơ', 'uô', 'ươ', 'ươi', 'ươu'],
				['uy', 'u', 'y', 'ui', 'uyu', 'iu', 'uya', 'uây'],
				['ưa', 'ư', 'a', 'ua', 'ia', 'au', 'uâ'],
				['uwa', 'ưa', 'ư', 'a', 'ua', 'ia', 'au', 'uâ'],
				['ưi', 'ư', 'i', 'ui', 'ưu', 'ươ', 'iu'],
				['uwi', 'ưi', 'ư', 'i', 'ui', 'ưu', 'ươ', 'iu'],
				['ươ', 'ư', 'ơ', 'ưu', 'ưi', 'uơ', 'uô'],
				['uwơ', 'ươ', 'ư', 'ơ', 'ưu', 'ưi', 'uơ', 'uô'],
				['ưow', 'ươ', 'ư', 'ơ', 'ưu', 'ưi', 'uơ', 'uô'],
				['uwow', 'ươ', 'ư', 'ơ', 'ưu', 'ưi', 'uơ', 'uô'],
				['ưu', 'ư', 'u', 'ươ', 'ưi', 'ui', 'uy'],
				['uwu', 'ưu', 'ư', 'u', 'ươ', 'ưi', 'ui', 'uy'],
				['yê', 'y', 'ê', 'iê', 'uê', 'iê'],
				['yee', 'yê', 'y', 'ê', 'iê', 'uê', 'iê'],
				['oai', 'oa', 'oi', 'ai', 'oai', 'ôi', 'ơi'],
				['oay', 'oa', 'ay', 'oai', 'ây'],
				['uây', 'uâ', 'uy', 'uya', 'ây'],
				['uaay', 'uây', 'uâ', 'uy', 'uya', 'ây'],
				['uôi', 'uô', 'ôi', 'ui', 'ươi', 'ươu'],
				['uooi', 'uôi', 'uô', 'ôi', 'ui', 'ươi', 'ươu'],
				['iêu', 'iê', 'iu', 'êu', 'yêu'],
				['ieeu', 'iêu', 'iê', 'iu', 'êu', 'yêu'],
				['uyê', 'uy', 'uê', 'yê', 'yêu', 'iêu'],
				['uyee', 'uyê', 'uy', 'uê', 'yê', 'yêu', 'iêu'],
				['ươu', 'ươ', 'ưu', 'uô', 'uơ', 'ươu'],
				['uwơu', 'ươu', 'ươ', 'ưu', 'uô', 'uơ', 'ươu'],
				['ưowu', 'ươu', 'ươ', 'ưu', 'uô', 'uơ', 'ươu'],
				['uwowu', 'ươu', 'ươ', 'ưu', 'uô', 'uơ', 'ươu'],
				['ươi', 'ươ', 'ưi', 'ơi', 'uô', 'ươi'],
				['uwơi', 'ươi', 'ươ', 'ưi', 'ơi', 'ươi'],
				['ưowi', 'ươi', 'ươ', 'ưi', 'ơi', 'ươi'],
				['uwowi', 'ươi', 'ươ', 'ưi', 'ơi', 'ươi'],
				['uya', 'ua', 'uy', 'uây'],
				['yêu', 'yê', 'êu', 'iêu', 'uyê'],
				['yeeu', 'yêu', 'yê', 'êu', 'iêu', 'uyê'],
				['uyu', 'uy', 'uya']
			]
phu_am_cuoi = [
				['c', 'ch'],
				['p'],
				['t'],
				['m', 'n'],
				['n', 'ng', 'm'],
				['ch', 'c', 'nh'],
				['ng', 'n', 'nh'],
				['nh', 'ch', 'n', 'ng'],
				['h', 'ch', 'nh'],
				['g', 'ng'],
				['', 'c', 'p', 't', 'm', 'n']
			]

dau_thanh = ['s', 'f', 'r', 'x', 'j']

phu_am_dau_sai = ['dd']
nguyen_am_sai = ['aw', 'aa', 'ee', 'oo', 'ow', 'uw', 'aau', 'aay', 'eeu', 'iee',
					'oaw', 'ooi', 'owi', 'uaa', 'uee', 'uoo', 'uow', 'uwa', 'uwi', 'uwu', 'yee',
					'uaay','uooi','ieeu','uyee','uwow','uwơu','ưowu','uwơi','ưowi','yeeu',
					'uwơ', 'ưow', 'uwowu', 'uwowi']

def tao_tap_nham_lan(am_tiet):
	am_tiet_khong_dau = convert(am_tiet)

	arr_dau = []
	arr_na = []
	arr_cuoi = []

	dau = ""
	na = ""
	cuoi = ""

	ko_pad = ""

	for i in xrange(len(dau_thanh)):
		if am_tiet_khong_dau.endswith(dau_thanh[i]):
			am_tiet_khong_dau = am_tiet_khong_dau[:len(am_tiet_khong_dau) - 1]
			
	ko_pad = am_tiet_khong_dau
	for i in xrange(len(phu_am_dau)):
		if am_tiet_khong_dau.startswith(phu_am_dau[i][0]):
			if phu_am_dau[i][0] in phu_am_dau_sai:
				dau = phu_am_dau[i][1]
				arr_dau = phu_am_dau[i][1:]
				ko_pad = am_tiet_khong_dau[len(phu_am_dau[i][0]):]
				break
			dau = phu_am_dau[i][0]
			arr_dau = phu_am_dau[i]
			ko_pad = am_tiet_khong_dau[len(phu_am_dau[i][0]):]
			break

	for i in xrange(len(phu_am_cuoi)):
		if ko_pad.endswith(phu_am_cuoi[i][0]):
			cuoi = phu_am_cuoi[i][0]
			arr_cuoi = phu_am_cuoi[i]
			ko_pad = ko_pad[:len(ko_pad) - len(phu_am_cuoi[i][0])]
			break

	for i in xrange(len(nguyen_am)):
		if ko_pad == nguyen_am[i][0]:
			if nguyen_am[i][0] in nguyen_am_sai:
				na = nguyen_am[i][1]
				arr_na = nguyen_am[i][1:]
				break
			na = nguyen_am[i][0]
			arr_na = nguyen_am[i]
			break

	arr_dau.append("")
	arr_cuoi.append("")
	

	arr_dau = sorted(list(set(arr_dau)))
	arr_na = sorted(list(set(arr_na)))
	arr_cuoi = sorted(list(set(arr_cuoi)))

	kq_temp = []
	kq = []

	# for i in xrange(len(arr_dau)):
	# 	for j in xrange(len(arr_na)):
	# 		for k in xrange(len(arr_cuoi)):
	# 			kq.append(arr_dau[i] + arr_na[j] + arr_cuoi[k])

	for i in xrange(len(arr_dau)):
		kq_temp.append(arr_dau[i] + na + cuoi)

	for i in xrange(len(arr_na)):
		kq_temp.append(dau + arr_na[i] + cuoi)

	for i in xrange(len(arr_cuoi)):
		kq_temp.append(dau + na + arr_cuoi[i])

	kq_temp = sorted(list(set(kq_temp)))


	for i in xrange(len(kq_temp)):
		kq.extend(them_dau(kq_temp[i]))

	kq = sorted(list(set(kq)))

	tu_dien = open("./vi-DauMoi.dic").read().lower()
	tu_dien = tu_dien.split()

	not_in_dic = []
	for i in xrange(len(kq)):
		if kq[i] not in tu_dien:
			not_in_dic.append(kq[i])

	for x in xrange(len(not_in_dic)):
		kq.remove(not_in_dic[x])

	kq = sorted(list(set(kq)))

	# for i in xrange(len(kq)):
	# 	print(kq[i])

	return kq

# tao_tap_nham_lan("với")