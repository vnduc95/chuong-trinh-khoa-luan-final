# -*- coding: utf-8 -*-

from __future__ import print_function
from keras.models import load_model
import numpy as np
import sys
import os

from tao_tap_nham_lan_am_tiet import tao_tap_nham_lan

# dictionary
text = open("chars.txt").read().lower()
text = text.split()
chars = sorted(list(set(text)))
chars.append('')

char_indices = dict((c, i) for i, c in enumerate(chars))
indices_char = dict((i, c) for i, c in enumerate(chars))

# len of LSTM input for training
maxlen = 5

def sample(preds, word):
    # return index of word has max probability in suggest word array
    preds = np.asarray(preds).astype('float64')
    preds = np.log(preds)
    exp_preds = np.exp(preds)
    preds = exp_preds / np.sum(exp_preds)

    arr_word = tao_tap_nham_lan(word)
    max_probability = 0
    index_of_true_word = -1
    for i in xrange(len(arr_word)):
        if (arr_word[i] in chars) and preds[char_indices[ arr_word[i] ]] > max_probability:
            max_probability = preds[char_indices[ arr_word[i] ]]
            index_of_true_word = char_indices[ arr_word[i] ]

    # return np.argmax(preds)
    return index_of_true_word


# load model LSTM
model = load_model("./model_input_5")


# ================================ CHECK ====================================

os.system('clear')
result_use_lstm = ''
false_para = open("./DataKiemThu/false.txt").read().lower()
sentences = false_para.split('\n')
for i in xrange(len(sentences)):
	words = sentences[i].split()
	new_sen = words[0]

	for j in xrange(1, len(words)):
		history = words[:j]
		index = j
		word_need_check = words[j]
		x = np.zeros((1, maxlen, len(chars)))

		if maxlen > len(history):
			for k in xrange(maxlen - len(history)):
				x[0, k, char_indices[""]] = 1.

			for t, word in enumerate(history):
				x[0, maxlen - len(history) + t, char_indices[word]] = 1.
		else:
			for t, word in enumerate(history):
				if t >= len(history) - maxlen:
					x[0, t - len(history) + maxlen, char_indices[word]] = 1.

		preds = model.predict(x, verbose=0)[0]
		true_index = sample(preds, word_need_check)
		true_word = indices_char[true_index]
		words[index] = true_word
		if true_word != word_need_check:
			print(word_need_check + ' => ' + true_word)

		new_sen = new_sen + ' ' + true_word

	if i == 0:
		result_use_lstm = new_sen
	else:
		result_use_lstm = result_use_lstm + "\n" + new_sen

text_file = open("./DataKiemThu/lstm.txt", "w")
text_file.write(result_use_lstm)
text_file.close()

# ================================ END CHECK ================================



# ================================= COMPARE =================================


# the true paragraph
true_para = open("./DataKiemThu/true.txt").read().lower()
# total number of word
n_word = len(true_para.split())
# total number of false word
n_total_false = 0
# number of word lstm check true
n_lstm_true = 0
# number of word lstm check false
n_lstm_false = 0
# number of word finding true
n_finding = 0

lstm_sentences = result_use_lstm.split('\n')
true_sentences = true_para.split('\n')
false_sentences = false_para.split('\n')
for i in xrange(len(true_sentences)):
	lstm_words = lstm_sentences[i].split()
	false_words = false_sentences[i].split()
	true_words = true_sentences[i].split()
	for j in xrange(len(lstm_words)):
		if false_words[j] != true_words[j]:
			n_total_false += 1
			if lstm_words[j] == true_words[j]:
				n_lstm_true += 1
			if lstm_words[j] != false_words[j]:
				n_finding += 1
		elif lstm_words[j] != true_words[j]:
			n_lstm_false += 1

os.system('clear')
print("Tổng số câu: " + str(len(true_sentences)))
print("Tổng số âm tiết: " + str(n_word))
print("Tổng số âm tiết sai: " + str(n_total_false))
print("Số âm tiết tìm kiếm chính xác: " + str(n_finding))
print("Số âm tiết kiểm tra chính xác: " + str(n_lstm_true))
print("Số âm tiết nhầm lẫn: " + str(n_lstm_false))
print("Tỷ lệ phát hiện âm tiết sai chính tả: " + str(int(n_lstm_true * 100. / n_total_false)) + '%')
print("Tỷ lệ nhần lẫn: " + str(int(n_lstm_false * 100. / (n_word - n_total_false))) + '%')


# =============================== END COMPARE ===============================