# -*- coding: utf-8 -*-

from __future__ import print_function
from keras.models import load_model
import numpy as np
import sys
import os

# dictionary
text = open("chars.txt").read().lower()
text = text.split()

chars = sorted(list(set(text)))
chars.append('')
print('total chars:', len(chars))
char_indices = dict((c, i) for i, c in enumerate(chars))
indices_char = dict((i, c) for i, c in enumerate(chars))

# len of input for training lstm
maxlen = 10

def sample(preds):
    # return index of word has max probability
    preds = np.asarray(preds).astype('float64')
    preds = np.log(preds)
    exp_preds = np.exp(preds)
    preds = exp_preds / np.sum(exp_preds)
    probas = np.random.multinomial(1, preds, 1)
    return np.argmax(preds)


# load model
model = load_model("./model_input_10")




# ================================ TEST ====================================

os.system('clear')
while (True):
    generated = ''
    print('Nhap cau: ')
    sentence = raw_input()
    sentence = sentence.lower()
    generated += sentence

    sys.stdout.write('=>   ' + generated)

    x = np.zeros((1, maxlen, len(chars)))
    for i in range(maxlen - len(sentence.split())):
        x[0, i, char_indices[""]] = 1.
    
    for t, char in enumerate(sentence.split()):
        x[0, maxlen - len(sentence.split()) + t, char_indices[char]] = 1.

    preds = model.predict(x, verbose=0)[0]
    next_index = sample(preds)
    next_char = indices_char[next_index]

    generated += next_char
    sentence = ' '.join(sentence.split()[1:] + [next_char])

    sys.stdout.write(' + ' + next_char + '\n\n\n')
    sys.stdout.flush()
    print()

# ================================ END TEST ================================