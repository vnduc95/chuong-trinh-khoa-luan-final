# -*- coding: utf-8 -*-

from __future__ import print_function
from keras.models import Sequential
from keras.models import load_model
from keras.layers import Dense, Activation
from keras.layers import LSTM
from keras.optimizers import RMSprop
from keras.utils.data_utils import get_file
import numpy as np
import random
import sys

# read from data file
sentences = open("./data_chuan.txt").read().lower().split('.')
print("So cau: ", len(sentences))

# dictionary
text = open("./chars.txt").read().lower()
text = text.split()

chars = sorted(list(set(text)))
chars.append('')
print('total chars:', len(chars))
char_indices = dict((c, i) for i, c in enumerate(chars))
indices_char = dict((i, c) for i, c in enumerate(chars))

historys = []
next_chars = []
maxlen = 10


# convert array into mang length = l
def convert(arr, l):
    result = [];
    for i in range(l):
        result.append("")

    if len(arr) > l:
        for i in xrange(0, l):
            result[i] = arr[len(arr) - l + i]
    else:
        for i in xrange(0, len(arr)):
            result[l - len(arr) + i] = arr[i]
    return result

# create historys and next_chars from data
for x in xrange(0, len(sentences)):
    words = sentences[x].split()
    if (len(words) > 1):
        for i in xrange(1, len(words)):
            historys.append(convert(words[0: i], maxlen))
            next_chars.append(words[i])


# ================================ TRAINING ============================


print('Vectorization...')
X = np.zeros((len(historys), maxlen, len(chars)), dtype=np.bool)
y = np.zeros((len(historys), len(chars)), dtype=np.bool)
for i, history in enumerate(historys):
    for t, char in enumerate(history):
        X[i, t, char_indices[char]] = 1

    y[i, char_indices[next_chars[i]]] = 1

# build the model: a single LSTM
print('Build model...')
model = Sequential()
model.add(LSTM(128, input_shape=(maxlen, len(chars))))
model.add(Dense(len(chars)))
model.add(Activation('softmax'))

optimizer = RMSprop(lr=0.01)
model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy'])

# load model
# model = load_model("./model_input_10")

# training
for iteration in range(1, 100):
    print()
    print('-' * 50)
    print('Iteration', iteration)
    model.fit(X, y,
              batch_size=128,
              epochs=1)

    model.save("./model_len_input_10".format(iteration))

# ================================ END TRAINING ============================